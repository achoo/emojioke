import * as Knex from 'knex';


export class FavouriteUserService{
    
    constructor(private knex:Knex) {
        
    }

    async getUser() {
        const user = await this.knex.select("*").from('favourite_users')
        return user
    }

    async addUser(body:any) {

        const newFollowedUser = {
            'user_id': body.userId,
            'followed_user_id': body.followedUserId
        }

        return this.knex.insert(newFollowedUser).into("favourite_users").returning("id")
    }

    async deleteUser(id:number){
        return this.knex('favourite_users')
        .where('id', id)
        .del()
    }
}