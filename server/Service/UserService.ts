import * as Knex from 'knex';
import { User } from '../interface';


export class UserService {

    constructor(private knex: Knex) {

    }

    async getUsers() {
        const users = await this.knex.select("*").from('users')
        return users
    }

    async getUsersByID(id:any) {
        const users = await this.knex.select("id","username","profile_pic").from('users').where('id', id);
        return users
    }

    async deleteUser(id: number) {
        if (id != undefined) {
            return this.knex('users')
                .where('id', id)
                .del()
        } else {
            return { result: 'invalid user' };
        }
    }

    async updateUser(id: number, body: User) {

        const newUser = {
            'username': body.username,
            'password': body.password,
            'email': body.email
        }
        return this.knex('users')
            .where("id", id)
            .update(
                newUser
            )
    }

    async uploadProfile(id:number, filePath: string) {
        await this.knex("users")
            .where('id ', id)
            .update('profile_pic ', filePath)
    }
}