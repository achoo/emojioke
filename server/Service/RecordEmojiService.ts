import * as Knex from 'knex';


export class RecordEmojiService {

    constructor(private knex: Knex) {

    }

    async getRecordEmojiById(id:number){
        const recordEmoji = await this.knex.select("*").from("song_records_emoji").where("song_record_id", id)
        return recordEmoji
    }


    async addRecordEmoji(id:number, body: any) {
        const newRecordEmoji = {
            'user_id': id,
            'emoji_id': body.emoji_id,
            'song_record_id': body.song_record_id,
            'clicked_time':body.clicked_time,
            'clicked_coord_x': body.clicked_coord_x,
            'clicked_coord_y': body.clicked_coord_y
        }
        return this.knex.insert(newRecordEmoji).into("song_records_emoji").returning("id")
    }
}