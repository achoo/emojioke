import * as Knex from 'knex';


export class CommentService{
    
    constructor(private knex:Knex) {
        
    }

    async getComments() {
        const comments = await this.knex.select("*").from('comments')
        return comments
    }

    async getCommentById(id:number){
        const comment = await this.knex.select("*").from("comments").where("song_record_id", id)
        return comment
    }


    async addComments(body: any) {
        const newComment = {
            'user_id': body.id,
            'song_record_id': body.song_record_id,
            'comments': body.comment,
            'time': body.time
        }
        return this.knex.insert(newComment).into("comments").returning("id")
    }

}