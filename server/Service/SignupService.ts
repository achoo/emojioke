import * as Knex from 'knex';
import {hashPassword} from '../hash'

export class SignupService {

    constructor(private knex: Knex) {

    }


    async addUser(username: string, password: string, email: string, profile_pic: string, level:string) {
        if (username != undefined || password != undefined || email != undefined) {
            console.log(password)
            const hash = await hashPassword(password)
            const newUser = {
                'username': username,
                'password': hash,
                'email': email,
                'profile_pic': profile_pic,
                'level': level
            }
            return this.knex.insert(newUser).into("users").returning("id")
        } else {
            return {result: 0};
        }
    }

}