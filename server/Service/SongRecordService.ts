import * as Knex from 'knex';


export class SongRecordService{
    
    constructor(private knex:Knex) {
        
    }

    async getSongRecord() {
        // const songRecords = await this.knex('song_records')
        // .join('users', 'song_records.user_id', 'users.id')
        // .join('songs', 'song_records.song_id', 'songs.id')
        // .leftJoin('song_records_emoji', 'song_records.id', 'song_records_emoji.song_record_id')
        // .leftJoin('emoji','song_records_emoji.emoji_id','emoji.id')
        //     .select('song_records.id',
        //     'users.username',
        //     'users.profile_pic',
        //     'songs.cover_image', 
        //     'songs.song_name',
        //     'song_records.filename',
        //     'song_records.created_at',
        //     'song_records_emoji.emoji_id',
        //     'emoji.value'
        //     ).orderBy('id')

        

            const result = await this.knex.raw(`
            select song_result.id,
                    users.username,
                    users.profile_pic,
                    songs.cover_image, 
                    songs.song_name,
                    song_records.filename,
                    song_records.created_at,
                    song_result.avg_value
            from (
                select song_records.id as id,
                    AVG(COALESCE(CAST(emoji.value as Float), 0)) as avg_value
                from songs 
                left join song_records on song_records.song_id = songs.id
                left join song_records_emoji on song_records.id = song_records_emoji.song_record_id 
                left join emoji on song_records_emoji.emoji_id = emoji.id 
                group by song_records.id
            ) as song_result
            inner join song_records on song_records.id = song_result.id
            inner join songs on song_records.song_id = songs.id  
            inner join users on song_records.user_id = users.id 
            order by song_result.avg_value desc
             `
            )
            if(result.rows[0] != null){
                return result.rows
            }
    }

    async getRecordByIdSong(id:number){
        const record = await 
        this.knex.select("song_records.id",
        "song_records.user_id", 
        "song_records.song_id",
        "song_records.filename",
        "songs.singer_id",
        "songs.song_name",
        "songs.genre",
        "songs.cover_image")
        .from("song_records")
        .where("song_records.id", id)
        .leftJoin('songs', 'song_records.song_id', 'songs.id')

        return record
    }


    async addSongRecord(body:any) {

        return this.knex.insert(body).into("song_records").returning("id")
         
    }

    async deleteSongRecord(id:number){
        return this.knex('song_records')
        .where('id', id)
        .del()
    }

}