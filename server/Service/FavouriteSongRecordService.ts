import * as Knex from 'knex';


export class FavouriteSongRecordService{
    
    constructor(private knex:Knex) {
        
    }

    async getFavouriteSong() {
        const favoriteSong = await this.knex.select("*").from('favourite_songs')
        return favoriteSong        
    }

    async addFavouriteSong(body: any) {
        const newFavouriteSong = {
            'user_id': body.user_id,
            'song_record_id': body.song_record_id,
        }
        return this.knex.insert(newFavouriteSong).into("favourite_songs").returning("id")
    }

    async deleteFavouriteSong(id:number){
        if (id != undefined) {
            return this.knex('favourite_songs')
                .where('id', id)
                .del()
        } else {
            return { result: 0 };
        }
    }
}