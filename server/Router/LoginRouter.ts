import * as express from 'express';
import {Request,Response} from 'express';
import { UserService } from '../Service/UserService';
import * as jwtSimple from 'jwt-simple';
import {jwtConfig} from '../jwt';
import { checkPassword} from '../hash';

export class LoginRouter{
    constructor(private userService:UserService){}
    router(){
        const router = express.Router();
        router.post('/',this.login);
        return router;
    }
    private login = async (req:Request,res:Response)=>{
        
        if (req.body.username && req.body.password) {
            const {username,password} = req.body;
            const user = (await this.userService.getUsers())
                    .find((user:any)=>
                        user.email == username &&
                        checkPassword(password,user.password)); 
            if (user) {
                const payload = {
                    id: user.id,
                    level: user.level
                };
                const token = jwtSimple.encode(payload, jwtConfig.jwtSecret);
                res.json({
                    token: token,
                    id: user.id
                });
            } else {
                res.sendStatus(401);
            }
        } else {
            res.sendStatus(401); 

        }
    }

}