import { UserRouter } from "./UserRouter";
import { UserService } from "../Service/UserService";

type Mockify<T> = {
    [P in keyof T]: jest.Mock<{}>;
  };

describe('userRouter', () => {

    let mockUserService:Mockify<UserService>;
    let userRouter: UserRouter;
    let req: any;
    let res: any;
  
    beforeEach(() => {
        mockUserService = {
            addUser: jest.fn( () => false),
            getUsers: jest.fn( () => [{
                id: 1,
                username: '',
                password: '',
                email: ''
            }]),
            updateUser: jest.fn(() => false),
            deleteUser: jest.fn(() => {})
        }
        req = {
            query: {},
            body: {
                username: 'kevin',
                password: 'kevin',
                email: 'kevin'
            },
            params: {
                id: 1
            },
            file: null
          };
          res = {
            status: jest.fn(() => {}),
            json: jest.fn(() => {}),
          };

        userRouter = new UserRouter(mockUserService as any)

    })

    it("should get all users" , async () => {
        await userRouter.get(req, res)
        expect(res.json).toBeCalledWith( [{
            id: 1,
            username: '',
            password: '',
            email: ''
        }])

    });

    xit("should add an user", async () => {
        await userRouter.post(req, res)
        expect(res.json).toBeCalledWith({result: "success"})
    })

    xit("should add an user failed", async () => {
        await userRouter.post(req, res)
        expect(res.json).toBeCalledWith({result: "failed"})
    })

    xit("should add an user failed if username, password, email is empty", async () => {
        await userRouter.post(req, res)
        expect(res.status).toBeCalledWith(500);
        expect(res.json).toBeCalledWith({result: "invalid user"})
    })

    it("should update an user", async () => {
        await userRouter.put(req, res)
        expect(res.json).toBeCalledWith({result:"update user"})
    })

    it("should update an user failed if the user is not exist", async () => {
        jest.spyOn(mockUserService, 'updateUser').mockReturnValue(false);
        await userRouter.put(req, res)
        expect(res.json).toBeCalledWith({result:"user not exist"})
    })

    it("should delete an user",async () => {
        jest.spyOn(mockUserService, 'deleteUser').mockReturnValue(true);
        await userRouter.delete(req, res);
        expect(res.json).toBeCalledWith({delete: 1})
    })

    it("should failed to detele an user if not exits", async () => {
        jest.spyOn(mockUserService, 'deleteUser').mockReturnValue(false);
        await userRouter.delete(req, res);
        expect(res.json).toBeCalledWith({delete: 0})

    })
})