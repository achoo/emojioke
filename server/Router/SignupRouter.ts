import * as express from 'express';

import {SignupService} from '../Service/SignupService';

export class SignupRouter {
    constructor(private signupService: SignupService){
        this.signupService = signupService
    }

    router() {
        const router = express.Router();
        router.post('/',this.post.bind(this));
        return router;
    }

    async post(req:express.Request, res: express.Response){
        if(req.body.username == '' || req.body.password == '' || req.body.email == '' || req.body.profile_pic == '' || req.body.level == ''){
            res.status(500)
            res.json({ result: "invalid input" })
            return
            
        }

        try {
            const result = await this.signupService.addUser(req.body.username, req.body.password, req.body.email, req.body.profile_pic, req.body.level)
            if(result.result == 0) {
                res.status(500)
                res.json({result: "signup fail"})
            } else {
                res.json({result: "signup success"})

            }
            

        } catch (e) {
            if(e){
                res.status(500)
                res.json({result: e})
                return
            }
        }

    }
    
}