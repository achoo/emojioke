import * as express from 'express';

import { RecordEmojiService } from '../Service/RecordEmojiService';

export class RecordEmojiRouter {
    constructor(private recordEmojiService: RecordEmojiService) {
    }

    router() {
        const router = express.Router();
        router.get('/:id', this.getRecordEmojiById.bind(this));
        router.post('/:id', this.addRecordEmoji.bind(this))
        return router;
    }


    async addRecordEmoji(req: express.Request, res: express.Response) {
        // console.log(req.params.id)
        try {
            console.log(req.body)
            const result = await this.recordEmojiService.addRecordEmoji(req.params.id, req.body)
            // console.log(result)
            res.json(result)

        } catch (e) {

            res.status(500)
            res.json({ result: 'fail' })
            return
        }

    }

    async getRecordEmojiById(req: express.Request, res: express.Response) {
        const recordEmoji = await this.recordEmojiService.getRecordEmojiById(req.params.id)
        res.json({isSuccess: 1, data: recordEmoji})
    }


}