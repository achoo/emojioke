import * as express from 'express';
import {FavouriteSongRecordService} from '../Service/FavouriteSongRecordService';

export class FavouriteSongRecordRouter {
    constructor(private favouriteSongService: FavouriteSongRecordService){
    }

    router() {
        const router = express.Router();
        router.get('/', this.getRecord.bind(this))
        router.post('/:id',this.addRecord.bind(this));
        router.delete('/:id',this.deleteRecord.bind(this));
        return router;
    }

    async getRecord(req:express.Request, res: express.Response){
        const song = await this.favouriteSongService.getFavouriteSong()
        res.json(song)
    }

    async addRecord(req:express.Request, res: express.Response){


        if (req.params.id == null || req.body.song_record_id == null) {
            res.status(500)
            res.json({ result: "empty input" })
            return
        }
        try {

            const newFovouriteRecord ={
                user_id: req.params.id,
                song_record_id: req.body.song_record_id,
            }

            const result = await this.favouriteSongService.addFavouriteSong(newFovouriteRecord)
            if(result[0] > 0){
                res.json({result: "success"})
            }

        } catch (e) {
            if(e){
                res.status(500)
                res.json({ result: 'add failed' })
                return
            }
        }

    }

    async deleteRecord(req:express.Request, res: express.Response){
        const result = this.favouriteSongService.deleteFavouriteSong(req.params.id);
        if(result) {
            res.json({delete: 1})
        } else {
            res.status(500)
            res.json({delete: 0})
        }
    }


    
}