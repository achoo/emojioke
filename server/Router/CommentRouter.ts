import * as express from 'express';
import {CommentService} from '../Service/CommentsService';

export class CommentRouter {
    constructor(private commentService: CommentService){
    }

    router() {
        const router = express.Router();
        router.get('/', this.getComment.bind(this))
        router.get('/:song_record_id',this.getCommentById.bind(this));
        router.post('/:id',this.postComment.bind(this));
        return router;
    }

    async getComment(req:express.Request, res: express.Response){
        const comment = await this.commentService.getComments()
        res.json(comment)
    }

    async getCommentById(req:express.Request, res:express.Response){
        const comment = await this.commentService.getCommentById(req.params.song_record_id)
        console.log(comment)
        if (comment[0] != null){
            
            res.json({isSuccess: 1, data: comment})
        } else {
            res.status(500)
            res.json({result: "comment_not_found"})
        }
    }

    async postComment(req:express.Request, res: express.Response){

        if(req.body.song_record_id == null || req.body.comment == '' || req.body.time == null){
            throw Error("empty input")
        }

        try {
            const newComment = {
                user_id: req.params.id,
                song_record_id: req.body.song_record_id,
                comment: req.body.comment,
                time: req.body.time

            }

            const result = await this.commentService.addComments(newComment)
            if(result[0] > 0){
                res.json({result: "add success"})
            }

        } catch (e) {
            if(e){
                res.status(500)
                res.json({ result: 'add failed' })
            }
        }

    }
    
}