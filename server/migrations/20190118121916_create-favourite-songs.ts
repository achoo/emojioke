import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('favourite_songs');
    if(!hasTable){
        return knex.schema.createTable('favourite_songs',(table)=>{
            table.increments();
            table.integer("user_id").notNullable();
            table.integer("song_record_id").notNullable();
            table.foreign("user_id").references("users.id");
            table.foreign("song_record_id").references("song_records.id")
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('favourite_songs');
};
