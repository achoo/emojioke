import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('comments');
    if(!hasTable){
        return knex.schema.createTable('comments',(table)=>{
            table.increments();
            table.integer("user_id").unsigned();
            table.integer("song_record_id").unsigned();
            table.foreign("user_id").references('users.id');
            table.foreign("song_record_id").references('song_records.id');
            table.string("comments");
            table.float('time');
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('comments');
};
