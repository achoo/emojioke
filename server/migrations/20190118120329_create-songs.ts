import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('songs');
    if(!hasTable){
        return knex.schema.createTable('songs',(table)=>{
            table.increments();
            table.integer("singer_id").unsigned();
            table.foreign("singer_id").references('singer.id');
            table.string("song_name").notNullable();
            table.enu('genre', ['Pop', 'Rock', 'Hip-Hop', 'EDM', 'Jazz', 'Classical']);
            table.string("filename").notNullable();
            table.string("cover_image");
        });  
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTableIfExists('songs');
};
