import * as express from 'express';
import * as bodyParser from 'body-parser';
import expressSession = require('express-session');
import passport = require('passport');
import * as cors from 'cors';

// import * as passportLocal from 'passport-local';

import * as Knex from 'knex';
import knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


import { LoginRouter } from './Router/LoginRouter';
import { SignupService } from './Service/SignupService';
import { SignupRouter } from './Router/SignupRouter';
import { UserService } from './Service/UserService';
import { UserRouter } from './Router/UserRouter';
import { SingerRouter } from './Router/SingerRouter';
import { SingerService } from './Service/SingerService';
import { SongRecordService } from './Service/SongRecordService';
import { SongService } from './Service/SongService';
import { SongRecordRouter } from './Router/SongRecordRouter';
import { SongRouter } from './Router/SongRouter';
import { FavouriteSongRecordService } from './Service/FavouriteSongRecordService';
import { FavouriteUserService } from './Service/FavouriteUserService';
import { EmojiRouter } from './Router/EmojiRouter';
import { EmojiService } from './Service/EmojiService';
import { FavouriteSongRecordRouter } from './Router/FavoriteSongRecordRouter';
import { FavouriteUserRouter } from './Router/FavoriteUserRouter';
import { CommentRouter } from './Router/CommentRouter';
import { CommentService } from './Service/CommentsService';
import { RecordEmojiService } from './Service/RecordEmojiService';
import { RecordEmojiRouter } from './Router/RecordEmojiRouter';




const signupService = new SignupService(knex)
export const userService = new UserService(knex)
const singerService = new SingerService(knex)
const songService = new SongService(knex)
const songRecordService = new SongRecordService(knex);
const favourRecordService = new FavouriteSongRecordService(knex)
const favourUserService = new FavouriteUserService(knex)
const commentService = new CommentService(knex)
const emojiService = new EmojiService(knex)
const recordEmojiService = new RecordEmojiService(knex)


const loginRouter = new LoginRouter(userService)
const signupRouter = new SignupRouter(signupService)
const userRouter = new UserRouter(userService)
const singerRouter = new SingerRouter(singerService)
const songRouter = new SongRouter(songService)
const songRecordRouter = new SongRecordRouter(songRecordService);
const favourRecordRouter = new FavouriteSongRecordRouter(favourRecordService)
const favourUserRouter = new FavouriteUserRouter(favourUserService)
const commentRouter = new CommentRouter(commentService)
const emojiRouter = new EmojiRouter(emojiService)
const recordEmojiRouter = new RecordEmojiRouter(recordEmojiService)

const app = express();
app.use(cors())
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());
import './passport'


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('assets'))
app.use(express.static('uploads'))



//////////////////////////////////////////////////////////// upload file

import * as multer from 'multer';


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const isLoggedIn = passport.authenticate('jwt', { session: false });


const upload = multer({ storage })
app.post('/uploads',isLoggedIn, express.Router().post('/') ,upload.single('mp3'), (req, res) => {
    console.log(req.user)
    console.log(req.file)
    const util = require('util');
    const exec = util.promisify(require('child_process').exec);

    async function convertVideo() {
        const uniqueAudioName = Date.now();
        const uniqueVideoName = Date.now();
        const uniqueFinalName = Date.now();
        await exec(`ffmpeg -i ${req.file.path} -i "./assets/OGSongs/extractedAudio/${req.file.originalname}.mp3" -filter_complex amerge -c:a libmp3lame -q:a 4 "./uploads/combinedAudio/${uniqueAudioName}.mp3"`, {
            env: {
                PATH: process.env.PATH
            }
        });
        await exec(`ffmpeg -i "./assets/OGSongs/${req.file.originalname}.mp4" -an "./uploads/strippedVideo/${uniqueVideoName}.mp4"`, {
            env: {
                PATH: process.env.PATH
            }
        });
        await exec(`ffmpeg -i "./uploads/strippedVideo/${uniqueVideoName}.mp4" -i "./uploads/combinedAudio/${uniqueAudioName}.mp3" -shortest -strict -2 "./uploads/finalEncodedFile/${uniqueFinalName}.mp4"`, {
            env: {
                PATH: process.env.PATH
            }
        });
        // stderr problem
        return ({isSuccess: 1, filename: uniqueFinalName })
    }
    const result = convertVideo();
    
    result.then(async(data) => {
        if(data.isSuccess){
            console.log(data)
            const user_id = Number(req.user.id)
            const song_id = Number(req.body.song_id)

            const newUploadRecord =  {
                user_id: user_id,
                song_id: song_id,
                filename: data.filename
            }
            console.log()
            // update song record to database

            const result = await songRecordService.addSongRecord(newUploadRecord)
            console.log(result)
            if(result[0] != null){
                res.json({isSuccess: 1, result: 'upload success'});
            } else {
                res.status(500)
                res.json({result: 'upload failed'})
            }
            
        }
    })
})


////////////////////////////////////////////////////////////// upload file


app.use('/login', loginRouter.router());
// passport.authenticate('local',{
//     failureMessage: 'failed to login',
//     successMessage: 'login success'
// }), (req, res) => {
//     res.json('login success')
// }
// async (req, res) => {
//     const Users = await userService.getUsers()
//     const user = Users.find((user:any) => {
//         if(user.email === req.body.email && user.password === req.body.password){
//             return true
//         } else {
//             return false
//         }
//     });

//     //if found user

//     if (typeof user !== 'undefined') {
//         if (typeof req.session != 'undefined') {
//             req.session.user = user
//             req.session.user = user.username
//             req.session.level = user.level
//             res.json({result: 'ok'})
//         } else {
//             res.status(500)
//             res.json({result: 'session_not_found'})
//         }
//     } else {
//         res.status(401)
//         res.json({result: 'not_authoized'})
//     }
// }




//                    vvvvvvvvvv      get user informaiton
//         console.log(req.user)

app.use('/signup', signupRouter.router());
app.use('/users',isLoggedIn, userRouter.router());
app.use('/singer', singerRouter.router());
app.use('/song', songRouter.router());
app.use('/songRecord', songRecordRouter.router());
app.use('/emoji', emojiRouter.router());
app.use('/favourRecord', isLoggedIn, favourRecordRouter.router());
app.use('/favourUser', isLoggedIn, favourUserRouter.router());
app.use('/comment', commentRouter.router());
app.use('/recordEmoji', recordEmojiRouter.router())

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});

