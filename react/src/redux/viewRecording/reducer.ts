import { ISelectedSongListState } from './state'
import { ISelectedSongActions } from './actions'

const initialState: ISelectedSongListState = {
  selectedSong: [{
    singer_id: '',
    song_name: '',
    genre: ''
  }],
  error: 'null'
}

export function viewSongRecord(state: ISelectedSongListState = initialState, action: ISelectedSongActions) {
  switch (action.type) {
    case "GET_SELECTED_SONG_SUCCESS":
      // alert(JSON.stringify(action))
      const newSongState = action.selectedSong.slice()
      return {
        selectedSong: newSongState,
        error: state.error
      };
    case "GET_SELECTED_SONG_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}