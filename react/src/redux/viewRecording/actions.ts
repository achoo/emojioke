import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { ISongState2 } from './state';

type GET_SELECTED_SONG_SUCCESS = 'GET_SELECTED_SONG_SUCCESS';
type GET_SELECTED_SONG_FAILED = 'GET_SELECTED_SONG_FAILED';

const {REACT_APP_API_SERVER} = process.env;

export function viewSongDetail(id:any):ThunkResult<void>{
    return async (dispatch:Dispatch<ISelectedSongActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/song/${id}`, {
            method: 'GET',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
        
        const result = await res.json()
        // alert(JSON.stringify( result.data) )
        // alert(JSON.stringify(result));
        if(result.isSuccess) {
            dispatch(getSongSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(getSongFailed(result))
        }
    }
}


export function getSongSucccess(song: any):IGetSelectedSongSuccessAction {
    return {
        type: 'GET_SELECTED_SONG_SUCCESS',
        selectedSong:song
    }
}

export function getSongFailed(message: string):IGetSelectedSongFailedAction {
    return {
        type: 'GET_SELECTED_SONG_FAILED',
        message
    }
}

interface IGetSelectedSongSuccessAction {
    type: GET_SELECTED_SONG_SUCCESS,
    selectedSong: ISongState2[]
};

interface IGetSelectedSongFailedAction {
    type: GET_SELECTED_SONG_FAILED,
    message: string
};

export type ISelectedSongActions = IGetSelectedSongSuccessAction| IGetSelectedSongFailedAction