import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { IEmojiRecordState } from './state';

type GET_EMOJI_RECORD_SUCCESS = 'GET_EMOJI_RECORD_SUCCESS';
type GET_EMOJI_RECORD_FAILED = 'GET_EMOJI_RECORD_FAILED';
type ADD_EMOJI_RECORD_SUCCESS = 'ADD_EMOJI_RECORD_SUCCESS';
type ADD_EMOJI_RECORD_FAILED = 'ADD_EMOJI_RECORD_FAILED';

const { REACT_APP_API_SERVER } = process.env;

export function viewSongEmojiRecord(id: any): ThunkResult<void> {
    return async (dispatch: Dispatch<IGetEmojiRecordActions>) => {


        const res = await fetch(`${REACT_APP_API_SERVER}/recordEmoji/${id}`, {
            method: 'GET',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });

        const result = await res.json()
        // alert(JSON.stringify(result));
        if (result.isSuccess) {

            const getEmoji = await fetch(`${REACT_APP_API_SERVER}/emoji/`, {
                method: 'GET',
                headers: {
                    // 'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
            const getEmojiResult = await getEmoji.json()
            // alert(JSON.stringify(getEmojiResult));
            if (getEmojiResult.isSuccess ) {
                const emojiList: any[] = []
                getEmojiResult.data.forEach((emoji: any) => {
                    let emojiCount = 0;
                    result.data.forEach((songRecordsEmoji: any) => {
                        if (songRecordsEmoji.emoji_id === emoji.id) {
                            emojiCount += 1;
                        }
                    })
                    emojiList.push({
                        id: emoji.id,
                        type: emoji.type,
                        value: emoji.value,
                        filename: emoji.filename,
                        emojiCount
                    });
                })

                dispatch(getEmojiRecordSucccess({
                    emojiList,
                    data: result.data
                }))
                // dispatch(getEmojiRecordSucccess(result.data as any))
            } else {
                alert("getEmojiResult.isSuccess is false")
                dispatch(getEmojiRecordFailed(result))
            }

            // dispatch(getEmojiRecordSucccess(result.data as any))
        } else {
            alert("result.isSuccess is false")
            // dispatch(getSongSucccess(result as any))
            dispatch(getEmojiRecordFailed(result))
        }
    }
}

export function addSongEmojiRecord(id: any , data:any): ThunkResult<void> {
    return async (dispatch: Dispatch<IAddEmojiRecordActions>) => {
        // alert(id)
        // alert( "Data received in  actions.ts addSongEmojiRecord method: "+ JSON.stringify(data) );
        // alert(JSON.stringify(data));
        const res = await fetch(`${REACT_APP_API_SERVER}/recordEmoji/${id}`, {
            method: 'POST',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const result = await res.json()
        // alert(JSON.stringify(result));
        if (result.result !== "fail") {
            // alert("result true")           
            dispatch(addEmojiRecordSucccess())
        } else {
            alert("result is false")
            dispatch(addEmojiRecordFailed(result))
        }
    }
}

export function getEmojiRecordSucccess(emojiRecords: any): IGetEmojiRecordSuccessAction {
    return {
        type: 'GET_EMOJI_RECORD_SUCCESS',
        emojiRecords
    }
}

export function getEmojiRecordFailed(message: string): IGetEmojiRecordFailedAction {
    return {
        type: 'GET_EMOJI_RECORD_FAILED',
        message
    }
}

export function addEmojiRecordSucccess(): IAddEmojiRecordSuccessAction {
    return {
        type: 'ADD_EMOJI_RECORD_SUCCESS'
    }
}

export function addEmojiRecordFailed(message: string): IAddEmojiRecordFailedAction {
    return {
        type: 'ADD_EMOJI_RECORD_FAILED',
        message
    }
}

interface IGetEmojiRecordSuccessAction {
    type: GET_EMOJI_RECORD_SUCCESS,
    emojiRecords: IEmojiRecordState
};

interface IGetEmojiRecordFailedAction {
    type: GET_EMOJI_RECORD_FAILED,
    message: string
};

interface IAddEmojiRecordSuccessAction {
    type: ADD_EMOJI_RECORD_SUCCESS,
};

interface IAddEmojiRecordFailedAction {
    type: ADD_EMOJI_RECORD_FAILED,
    message: string
};

export type IGetEmojiRecordActions = IGetEmojiRecordSuccessAction | IGetEmojiRecordFailedAction
export type IAddEmojiRecordActions = IAddEmojiRecordSuccessAction | IAddEmojiRecordFailedAction





