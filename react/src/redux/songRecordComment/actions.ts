import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { ICommentState } from './state';

type GET_COMMENT_SUCCESS = 'GET_COMMENT_SUCCESS';
type GET_COMMENT_FAILED = 'GET_COMMENT_FAILED';

const {REACT_APP_API_SERVER} = process.env;

export function viewSongRecordComment(id:any):ThunkResult<void>{
    return async (dispatch:Dispatch<IGetCommentActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/comment/${id}`, {
            method: 'GET',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
        
        const result = await res.json()
        // alert(JSON.stringify(result));
        if(result.isSuccess) {
            dispatch(getCommentSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(getCommentFailed(result))
        }
    }
}


export function getCommentSucccess(comments: any):IGetCommentSuccessAction {
    return {
        type: 'GET_COMMENT_SUCCESS',
        comments
    }
}

export function getCommentFailed(message: string):IGetCommentFailedAction {
    return {
        type: 'GET_COMMENT_FAILED',
        message
    }
}

interface IGetCommentSuccessAction {
    type: GET_COMMENT_SUCCESS,
    comments: ICommentState[]
};

interface IGetCommentFailedAction {
    type: GET_COMMENT_FAILED,
    message: string
};

export type IGetCommentActions = IGetCommentSuccessAction| IGetCommentFailedAction









type INSERT_COMMENT_SUCCESS = 'INSERT_COMMENT_SUCCESS';
type INSERT_COMMENT_FAILED = 'INSERT_COMMENT_FAILED';


export function insertSongRecordComment(id:any):ThunkResult<void>{
    return async (dispatch:Dispatch<IInsertCommentActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/comment/${id}`, {
            method: 'POST',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
        
        const result = await res.json()
        // alert(JSON.stringify(result));
        if(result.isSuccess) {
            dispatch(insertCommentSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(insertCommentFailed(result))
        }
    }
}


export function insertCommentSucccess(comments: any):IInsertCommentSuccessAction {
    return {
        type: 'INSERT_COMMENT_SUCCESS',
        comments
    }
}

export function insertCommentFailed(message: string):IInsertCommentFailedAction {
    return {
        type: 'INSERT_COMMENT_FAILED',
        message
    }
}

interface IInsertCommentSuccessAction {
    type: INSERT_COMMENT_SUCCESS,
    comments: ICommentState[]
};

interface IInsertCommentFailedAction {
    type: INSERT_COMMENT_FAILED,
    message: string
};

export type IInsertCommentActions = IInsertCommentSuccessAction| IInsertCommentFailedAction