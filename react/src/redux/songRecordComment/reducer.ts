import { ICommentListState } from './state'
import { IGetCommentActions } from './actions'

const initialState: ICommentListState = {
  comments: [],
  error: 'null'
}

export function viewSongRecordComment(state: ICommentListState = initialState, action: IGetCommentActions) {
  switch (action.type) {
    case "GET_COMMENT_SUCCESS":
      // alert(JSON.stringify(action))
      const newSongState = action.comments.slice()
      return {
        comments: newSongState,
        error: state.error
      };
    case "GET_COMMENT_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}

export function addSongRecordComment(state: ICommentListState = initialState, action: IGetCommentActions) {
  switch (action.type) {
    case "GET_COMMENT_SUCCESS":
      // alert(JSON.stringify(action))
      const newSongState = action.comments.slice()
      return {
        comments: newSongState,
        error: state.error
      };
    case "GET_COMMENT_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}