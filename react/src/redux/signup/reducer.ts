import { ISignupState } from './state'
import { ISignActions } from './action';

const initialState = {
    isSignup: false
};

export function signReducer(state: ISignupState = initialState, action: ISignActions) {
    switch (action.type) {
        case "SIGNUP_SUCCESS":
            return Object.assign({}, state, {
                isSignup: true
            });
        case "SIGNUP_FAILED":
            return Object.assign({}, state, {
                isSignup: true
            });
        default:
            return state;
    }
}