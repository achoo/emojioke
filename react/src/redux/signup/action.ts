import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { push, CallHistoryMethodAction } from 'connected-react-router';

type SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
type SIGNUP_FAILED = 'SIGNUP_FAILED';

const {REACT_APP_API_SERVER} = process.env;

export function signup(username:string, password:string, email:string):ThunkResult<void>{
    return async (dispatch:Dispatch<ISignActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/signup`,{
            method: 'POST',
            headers: {
                "Content-Type":"application/json; charset=utf-8"
            },
            body: JSON.stringify({username, password, email})
        })

        const result = await res.json()
        if (result.result === 'signup success') {
            dispatch(signupSuccess())
            dispatch(push('/login'))
        } else {
            dispatch(signupFailed(result.result))
        }

    }
}


export function signupSuccess():ISignupSuccessAction{
    return {
        type: 'SIGNUP_SUCCESS',

    }
}

export function signupFailed(message: string):ISignupFailedAction{
    return {
        type: 'SIGNUP_FAILED',
        message
    }
}

interface ISignupSuccessAction{
    type: SIGNUP_SUCCESS,
}

interface ISignupFailedAction {
    type: SIGNUP_FAILED,
    message: string
};
  
export type ISignActions = ISignupSuccessAction | ISignupFailedAction;