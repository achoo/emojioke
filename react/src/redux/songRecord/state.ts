export interface ISongRecordState {
   user_id: number
   song_id: number 
   filename: string 
   created_at: string
}

export interface ISongRecordListState {
   songRecord: ISongRecordState
   error: string ;
}