export interface ISongState {
    id:string,
    song_name: string ;
    genre: string ,
    filename: string,
    cover_image: string,
    name: string,
    singer_gender: ''
}

export interface ISongListState {
    songList: ISongState[]
    error: string ;
}