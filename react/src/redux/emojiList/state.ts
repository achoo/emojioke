export interface IEmojiState {
   id: string
   type: string 
   value: number
   filename: string 
}

export interface IEmojiListState {
   emojiList: IEmojiState[]
   error: string ;
}