import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';
import { IEmojiState } from './state';

type GET_EMOJI_LIST_SUCCESS = 'GET_EMOJI_LIST_SUCCESS';
type GET_EMOJI_LIST_FAILED = 'GET_EMOJI_LIST_FAILED';

const {REACT_APP_API_SERVER} = process.env;

export function getEmojiList():ThunkResult<void>{
    return async (dispatch:Dispatch<IEmojiListActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/emoji/`, {
            method: 'GET',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
        
        const result = await res.json()
        // alert(JSON.stringify(result));
        if(result.isSuccess) {
            dispatch(getEmojiListSucccess(result.data as any))
        } else {
            // dispatch(getSongSucccess(result as any))
            dispatch(getEmojiListFailed(result))
        }
    }
}


export function getEmojiListSucccess(list: any):IGetEmojiListSuccessAction {
    return {
        type: 'GET_EMOJI_LIST_SUCCESS',
        emojiList:list
    }
}

export function getEmojiListFailed(message: string):IGetEmojiListFailedAction {
    return {
        type: 'GET_EMOJI_LIST_FAILED',
        message
    }
}

interface IGetEmojiListSuccessAction {
    type: GET_EMOJI_LIST_SUCCESS,
    emojiList: IEmojiState[]
};

interface IGetEmojiListFailedAction {
    type: GET_EMOJI_LIST_FAILED,
    message: string
};

export type IEmojiListActions = IGetEmojiListSuccessAction| IGetEmojiListFailedAction