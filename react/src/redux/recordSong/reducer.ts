import { IImportSongState } from './state';
import { IRecordSongActions } from './action';

const initState: IImportSongState = {
    songImport: [{
        fluid: true,
        controls: true,
        sources: [{
            src: '',
            type: ''
        }]
    }],
    error: 'null'
}

export function recordSongReducer(state: IImportSongState = initState, action: IRecordSongActions) {
    switch (action.type) {
        case "IMPORT_SONG_SUCCESS":
            const newImportState = action.songImport.slice()
            return {
                songImport: newImportState,
                error: state.error
            };
        case "IMPORT_SONG_FAILED":
            return Object.assign({}, state, action.message);
            ;
        default:
            return state;
    }
}