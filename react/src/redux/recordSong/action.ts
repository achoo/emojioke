import { Dispatch } from 'redux';
import { IrecordSong } from './state';

export type IMPORT_SONG_SUCCESS = 'IMPORT_SONG_SUCCESS';
export type IMPORT_SONG_FAILED = 'IMPORT_SONG_FAILED';

const { REACT_APP_API_SERVER } = process.env;

export function importSong(filename: string): any {
    return (dispatch: Dispatch<IRecordSongActions>) => {
        const newVideo = [{
            fluid: true,
            controls: true,
            sources: [{
                src: `${REACT_APP_API_SERVER}/OGSongs/${filename}.mp4`,
                type: 'video/mp4'
            }]
        }]

        dispatch(importSongSuccess(newVideo))
    }
}

export function importSongSuccess(video: IrecordSong[]): IImportSongSuccessAction {
    return {
        type: 'IMPORT_SONG_SUCCESS',
        songImport: video
    }
}

export function importSongFailed(message: string): IImportSongFailedAction {
    return {
        type: 'IMPORT_SONG_FAILED',
        message
    }
}

interface IImportSongSuccessAction {
    type: IMPORT_SONG_SUCCESS,
    songImport: IrecordSong[]
};

interface IImportSongFailedAction {
    type: IMPORT_SONG_FAILED,
    message: string
};

export type IRecordSongActions = IImportSongSuccessAction | IImportSongFailedAction