import { ThunkResult } from 'src/Store';
import { Dispatch } from 'redux';

type ADD_EMOJI_RECORD_SUCCESS = 'ADD_EMOJI_RECORD_SUCCESS';
type ADD_EMOJI_RECORD_FAILED = 'ADD_EMOJI_RECORD_FAILED';

const { REACT_APP_API_SERVER } = process.env;

export function addSongEmojiRecord(id: any , data:any): ThunkResult<void> {
    return async (dispatch: Dispatch<IAddEmojiRecordActions>) => {
        // alert(id)
        // alert(JSON.stringify(data));
        const res = await fetch(`${REACT_APP_API_SERVER}/recordEmoji/${id}`, {
            method: 'POST',
            headers: {
                // 'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(data)
        });

        const result = await res.json()
        // alert(JSON.stringify(result));
        if (result.result !== "fail") {
            // alert("result true")           
            dispatch(addEmojiRecordSucccess())
        } else {
            alert("result is false")
            dispatch(addEmojiRecordFailed(result))
        }
    }
}

export function addEmojiRecordSucccess(): IAddEmojiRecordSuccessAction {
    return {
        type: 'ADD_EMOJI_RECORD_SUCCESS'
    }
}

export function addEmojiRecordFailed(message: string): IAddEmojiRecordFailedAction {
    return {
        type: 'ADD_EMOJI_RECORD_FAILED',
        message
    }
}

interface IAddEmojiRecordSuccessAction {
    type: ADD_EMOJI_RECORD_SUCCESS,
};

interface IAddEmojiRecordFailedAction {
    type: ADD_EMOJI_RECORD_FAILED,
    message: string
};

export type IAddEmojiRecordActions = IAddEmojiRecordSuccessAction | IAddEmojiRecordFailedAction