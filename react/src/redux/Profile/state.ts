export interface IUserRecordState {
   id: number | null
   username: string 
   email: string 
   profile_pic: string
}

export interface IUserRecordListState {
   user: IUserRecordState[]
   error: string ;
}