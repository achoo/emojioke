import { IUserRecordListState } from './state'
import { IGetUserActions } from './actions'

const initialState: IUserRecordListState = {
  user: [{
    id: null,
    username: '' ,
    email: '' ,
    profile_pic: ''
  }],
  error: 'null'
}

export function getUserInformation(state: IUserRecordListState = initialState, action: IGetUserActions) {
  switch (action.type) {
    case "GET_USER_SUCCESS":
      // alert(JSON.stringify(action))
      const newUser = action.user
      return {
        user: newUser,
        error: state.error
      };
    case "GET_USER_FAILED":
      return Object.assign({}, state, action.message);
      ;
    default:
      return state;
  }
}