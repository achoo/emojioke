import thunk, { ThunkDispatch, ThunkAction } from 'redux-thunk';
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import logger from 'redux-logger';
import { IAuthState } from './redux/auth/state';
import { IAuthActions } from './redux/auth/action';
import { authReducer } from './redux/auth/reducer';
import { ISignupState } from './redux/signup/state';
import { signReducer } from './redux/signup/reducer';
import { ISignActions } from './redux/signup/action';
import { recordSongReducer } from './redux/recordSong/reducer';
import { IImportSongState } from './redux/recordSong/state';
import { createBrowserHistory } from 'history';
import { connectRouter, RouterState, routerMiddleware } from 'connected-react-router';
import { ISongListState } from './redux/songSelect/state';
import { songReducer } from './redux/songSelect/reducer';
// view song record
import { viewSongRecord }  from './redux/viewRecording/reducer';
import { ISelectedSongListState } from './redux/viewRecording/state';
// song record comment
import { viewSongRecordComment }  from './redux/songRecordComment/reducer';
import { ICommentListState } from './redux/songRecordComment/state';
// emoji record
import { viewSongEmojiRecord }  from './redux/songRecordEmoji/reducer';
import { IEmojiRecordListState } from './redux/songRecordEmoji/state';
// emoji List
import { viewEmojiList }  from './redux/emojiList/reducer';
import { IEmojiListState } from './redux/emojiList/state';
// song record
import { viewSongRecording }  from './redux/songRecord/reducer';
import { ISongRecordListState } from './redux/songRecord/state';
// user record
import { getUserInformation }  from './redux/Profile/reducer';
// import { IUserRecordListState } from './redux/Profile/state';

export interface IRootState {
   auth: IAuthState,
   sign: ISignupState,
   importSong: IImportSongState,
   songList: ISongListState,
   router: RouterState,
   selectedSong: ISelectedSongListState,
   songRecordComment: ICommentListState,
   songEmojiRecord: IEmojiRecordListState,
   emojiList: IEmojiListState,
   songRecord: ISongRecordListState,
   recordList: any
   user:any
}

export const history = createBrowserHistory();

export const rootReducer = combineReducers<IRootState>({
   auth: authReducer,
   sign: signReducer,
   importSong: recordSongReducer,
   songList: songReducer,
   router: connectRouter(history),
   selectedSong: viewSongRecord,
   songRecordComment: viewSongRecordComment,
   songEmojiRecord : viewSongEmojiRecord,
   emojiList: viewEmojiList,
   songRecord: viewSongRecording,
   recordList: viewSongRecording,
   user: getUserInformation
})

type IRootAction = IAuthActions | ISignActions;

declare global {
   /* tslint:disable:interface-name */
   interface Window {
      __REDUX_DEVTOOLS_EXTENSION__: any
   }
}

export type ThunkResult<R> = ThunkAction<R, IRootState, null, IRootAction>

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>


export default createStore<IRootState, IRootAction, {}, {}>(
   rootReducer,
   compose(
      applyMiddleware(thunk),
      applyMiddleware(logger),
      applyMiddleware(routerMiddleware(history)),
      // REDUX_DEVTOOLS_EXTENSION only works in chrome ,so have error in safira and firefox
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
   )
)