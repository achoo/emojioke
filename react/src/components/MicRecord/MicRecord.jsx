import React from 'react';
import AudioAnalyser from './AudioAnalyser';
import UploadRecord from './UploadRecord'


// interface IMicState {
//   audio: any,
//   audioHistory: string[],
//   isStart: boolean
// }

class MicExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      audio: null,
      audioHistory: []
    };
  }

   async getMicrophone() {
    const audio = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: false
    })
    this.setState({ audio, isStart: true });

  }

   stopMicrophone() {
    this.state.audio.getTracks().forEach((track) => track.stop());
    this.state.audioHistory.push(this.state.audio)
    this.setState({ audio: null, isStart: false });

    // const recorder = new MediaRecorder(this.state.audioHistory[0])
    // let chunks = [];
    // console.log(recorder)
    // console.log(recorder.ondataavailable)

    // recorder.ondataavailable = function(ev) {
    //     chunks.push(ev.data);
    //     console.log(chunks)
    // }
  }

   toggleMicrophone = () => {
    if (this.state.audio) {
      this.stopMicrophone();
    } else {
      this.getMicrophone();
    }
  }


   render() {
    return (
      <div className="App">
        {/* <div className="controls">
          <button onClick={this.toggleMicrophone}>
            {this.state.audio ? 'Stop microphone' : 'Get microphone input'}
          </button>

          {this.state.audio ? <AudioAnalyser audio={this.state.audio} /> : ''}

        </div> */}

        <UploadRecord />
      </div>
    );
  }
}

export default MicExample