// export type IBoard = Array<string|null>
// export type IBoardHistory = IBoard[]

// export interface IBoardState{
//     boardHistory: IBoardHistory
// }


export interface IVideoItemProps {
   user_id: number,
   song_id: number,
   filename: string
}