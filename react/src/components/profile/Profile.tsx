import * as React from 'react';
import './profile.css';
import { ThunkDispatch, IRootState } from 'src/Store';
import { connect } from 'react-redux'
import { Button } from 'reactstrap';

import { getUser, uploadProfile } from 'src/redux/Profile/actions'
import { match } from 'react-router-dom';



interface IUserProps {
    match: match,
    user: []
    getUser: (id: number) => void,
    uploadProfile: (id: number, file: File) => void,
}

interface IUserState {
    file: File
}


class Profile extends React.Component<IUserProps, IUserState> {
    constructor(props: IUserProps) {
        super(props)

    }

    public async componentDidMount() {
        const userId = Number(localStorage.getItem("user"))
        this.props.getUser(userId);
    }


    private handleFileOnChange = (files: FileList) => {
        this.setState({ file: files[0] })
    }

    private handleFileOnClick = async (id: number) => {
        await this.props.uploadProfile(id, this.state.file);
        this.props.getUser(id);
    }


    public render() {
        return (
            <div className="profileDiv">
                {
                    this.props.user.map((user: any, index: any) => 
                        <form key={index} >
                            <div className="center">
                                <h4 style={{ "fontFamily": "Shadows Into Light Two Regular" }}>Your Profile</h4>
                            </div>
                            <div className="container">
                                <label>
                                    <b >Username</b>
                                </label>
                                <p style={{ "fontFamily": "Shadows Into Light Two Regular" }} key={index}>{user.username}</p>
                                <label>
                                    <b id='profilePictureText'>Profile Picture</b>
                                </label>
                                <div className="center">
                                    <div className="imgcontainer">
                                        <p key={index}>{user.name}</p>
                                        {user.profile_pic?<img src={`${process.env.REACT_APP_API_SERVER}/images/profile_pictures/${user.profile_pic}` } id="profilePic"/>:""}
                                    </div>
                                    <div>
                                        {/*  tslint:disable-next-line jsx-no-lambda */}
                                        <input style={{ "fontFamily": "Shadows Into Light Two Regular" }} id='inputButton' type='file' name="profile" onChange={(e) => this.handleFileOnChange(e.target.files as FileList)} />
                                        <Button id="uploadButton" color='info' onClick={this.handleFileOnClick.bind(null, user.id)}> Upload</Button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    )
                }

            </div>
        );
    }
}


const mapStateToProps = (state: IRootState) => ({
    user: state.user.user,
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getUser: (id: number) => { dispatch(getUser(id)) },
    uploadProfile: (id: number, file: File) => dispatch(uploadProfile(id, file))
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

