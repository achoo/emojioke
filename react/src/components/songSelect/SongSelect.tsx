import * as React from 'react';
import './SongSelect.css';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux'
import { ThunkDispatch, IRootState } from 'src/Store';
import { getSong } from 'src/redux/songSelect/action';
import { ISongState } from 'src/redux/songSelect/state';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrophoneAlt } from '@fortawesome/free-solid-svg-icons';
library.add(faMicrophoneAlt);

interface ISongSelectState {
    selectedSong: any,
    modal: boolean,
    dropdownOpen: boolean
}

interface ISongSelectProps {
    song: ISongState[]
    getSong: () => void
}

class SongSelect extends React.Component<ISongSelectProps, ISongSelectState> {
    constructor(props: ISongSelectProps) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.state = {
            selectedSong: null,
            modal: false,
            dropdownOpen: false,
        }

        this.back = this.back.bind(this);
    }

    public toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }


    public componentDidMount() {

        this.props.getSong();
    }

    public handleClick = (e: any) => {
        const selectedSong = this.props.song.find((song) => song.song_name === e.target.alt)
        this.setState({
            modal: true,
            selectedSong
        })
    }

    private renderModal(song: any) {
        return <Modal isOpen={this.state.modal}  >
            <ModalHeader>
                <img src={`${process.env.REACT_APP_API_SERVER}/images/cover_images/${song.cover_image}.jpg`} className="coverImgModal" />
            </ModalHeader>
            <ModalBody>
                Song Name: {song.song_name}
            </ModalBody>
            <ModalBody>
                Genre: {song.genre}
            </ModalBody>
            <ModalBody>
                Singer:  {song.name}
            </ModalBody>
            <ModalBody>
                Gender:  {song.gender}
            </ModalBody>
            <ModalFooter>
                <Button color="info" >
                    <Link className="modalLink" to={`record_song/${song.filename}/${song.id}`}>Sing</Link>
                </Button>
                <Button className="modalLink" color="secondary" onClick={this.back}>Cancel</Button>
            </ModalFooter>
        </Modal>
    }

    private back() {
        this.setState({
            modal: !this.state.modal
        });
    }

    public render() {
        return (
            <div id="selectSongBackgroundImage">
                <Dropdown id="dropdownHome" isOpen={this.state.dropdownOpen} size="sm" toggle={this.toggle}>
                    <DropdownToggle caret>
                        Filter Search
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem header>By Genre</DropdownItem>
                        <DropdownItem>Some Action</DropdownItem>
                        <DropdownItem disabled>Action (disabled)</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>Pop</DropdownItem>
                        <DropdownItem>Rock</DropdownItem>
                        <DropdownItem>Hip-Hop</DropdownItem>
                        <DropdownItem>EDM</DropdownItem>
                        <DropdownItem>Jazz</DropdownItem>
                        <DropdownItem>Classical</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem header>By Rating</DropdownItem>
                        <DropdownItem>Some Action</DropdownItem>
                        <DropdownItem disabled>Action (disabled)</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>5 Stars</DropdownItem>
                        <DropdownItem>4 Stars</DropdownItem>
                        <DropdownItem>3 Stars</DropdownItem>
                        <DropdownItem>2 Stars</DropdownItem>
                        <DropdownItem>1 Star</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
                <div className="songSelectContainer">
                    <h5 style={{ "fontFamily": "Kenwave Regular" }} className="text-white center" id="songSelectionHeading">Sing Song Selection</h5>
                    {
                        this.props.song.map((song, index) =>
                            <Container className="grid-container" id="selectSongBackgroundTexture" key={index}>
                                <Row id="selectSongSelectionRow">
                                    <Col sm={{ size: 'auto' }} className="grid-item songItem2 card">
                                        <div className="selectSongDiv responsive card-image">
                                            <img className="coverImg z-depth-2 hoverable" alt={song.song_name} onClick={this.handleClick} src={`${process.env.REACT_APP_API_SERVER}/images/cover_images/${song.cover_image}.jpg`} />
                                            <Container Container sm={{ size: 'auto' }} id='textDiv'>
                                                <h6 style={{ "fontFamily": "Shadows Into Light Two Regular" }} className="songListText ">Song: {song.song_name}</h6>
                                                <h6 style={{ "fontFamily": "Shadows Into Light Two Regular" }} className="songListText ">Artist: {song.name}</h6>
                                            </Container>
                                        </div>
                                    </Col>
                                    <Col m={{ size: 'auto' }} className="songItem2 grid-item buttonDiv">
                                        <Link className="modalLink" id="linkButton" to={`record_song/${song.filename}/${song.id}`}>
                                            <Button color="info songListButton" > Sing <FontAwesomeIcon icon="microphone-alt" /></Button>
                                        </Link>
                                    </Col>
                                </Row>
                            </Container>
                        )
                    }
                    {this.state.modal ? this.renderModal(this.state.selectedSong) : ""}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: IRootState) => ({
    song: state.songList.songList
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getSong: () => {
        dispatch(getSong())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(SongSelect);