import * as React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import './SongSelectmodal.css';

interface ISongSelectModalProps {
    // tryAgain: ()=> void
    // backToHome: ()=> void
    song: any
}

class SongSelectModal extends React.Component<ISongSelectModalProps, {}>{
    // private modal: any
    // constructor(props: any) {
    //     super(props)
    //     this.modal = React.createRef()
    // }
    public render() {
        console.log(this.props.song)
        return (
            <Modal isOpen={true} >
                <ModalHeader>
                <img src={`${process.env.REACT_APP_API_SERVER}/images/cover_images/${this.props.song.cover_image}.jpg`} alt="cover image" className="modalImg z-depth-2" />
                </ModalHeader>
                <ModalBody>
                   Song Name: {this.props.song.song_name}
                </ModalBody>
                <ModalBody>
                   Genre: {this.props.song.genre}
                </ModalBody>
                <ModalBody>
                  Artist:  {this.props.song.name}
                </ModalBody>
                <ModalBody>
                  Singers Gender:  {this.props.song.gender}
                </ModalBody>
                <ModalFooter>
                    <Link to={`record_song/${this.props.song.filename}/${this.props.song.id}`}>Select</Link>
                </ModalFooter>
                <Button color="secondary">Back</Button>{' '}
            </Modal>
        )
    }
}


export default SongSelectModal;