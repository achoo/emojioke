import * as React from 'react';
import './footer.css';
import Logout from '../../Logout';
import { Container, Row } from 'reactstrap';

const Footer = () => {
    return (
        <nav id="footerBackgroundImage">
            <Container>
                <Row>
                <Logout />
                </Row>
            </Container>
        </nav>
    )
}

export default Footer;
