import * as React from 'react';
import './Login.css'
import { login } from 'src/redux/auth/action';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from 'src/Store';
import loginImg from '../../assets/images/login.png';
import { Button } from 'reactstrap';
// import drawingEmoji from '../../assets/images/drawing_emoji2.png';
// import { connect } from 'react-redux'

interface ILoginState {
  username: string,
  password: string
}

interface ILoginProps {
  login: (username: string, password: string) => void,
}



class Login extends React.Component<ILoginProps, ILoginState> {
  constructor(props: ILoginProps) {
    super(props);
    this.state = {
      username: "",
      password: ""
    }
  }


  private onSubmit = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    this.props.login(this.state.username, this.state.password)
  }


  private onChange = (field: 'username' | 'password', e: React.FormEvent<HTMLInputElement>) => {
    const state = {};
    state[field] = e.currentTarget.value;
    this.setState(state);
  }

  public render() {
    return (
      <div id="loginBackgroundImage">
        <form>
          <div className="center">
            <h4 style={{ "fontFamily": "Shadows Into Light Two Regular" }}>Login</h4>
          </div>
          <div className="imgcontainer">
            <img src={loginImg} alt="" className="loginImg" />
          </div>

          <div className="loginContainer">

            <label>
              <b>Email</b>
            </label>
            <input type="text"
              placeholder="Enter Email"
              value={this.state.username}
              onChange={this.onChange.bind(this, 'username')}
              required />

            <label>
              <b>Password</b>
            </label>
            <input type="password"
              placeholder="Enter Password"
              value={this.state.password}
              onChange={this.onChange.bind(this, 'password')}
              required />

            <Button color="info" type="submit" onClick={this.onSubmit}>Login</Button>
          </div>
        </form>
      </div>
    );
  }
}


const mapStateToProps = (state: IRootState) => ({});


const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  login: (username: string, password: string) => dispatch(login(username, password)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Login)
