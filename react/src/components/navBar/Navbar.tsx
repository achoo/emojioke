import * as React from 'react';
import './NavBar.css';
import { Link, NavLink } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGrinStars, faHome, faMicrophoneAlt, faMusic, faSignInAlt, faUserPlus, faUser } from '@fortawesome/free-solid-svg-icons';
library.add(faGrinStars, faHome, faMicrophoneAlt, faMusic, faSignInAlt, faUserPlus, faUser);

const Navbar = () => {
    return (
        <nav id="navBarBackgroundImage">
            <Container>
                <Row id="navRow">
                    <Col sm={{ size: 'auto' }}>
                        <h3 style={{ "fontFamily": "Kenwave Regular" }} className="logo" id="heading">Emoji<Link to="/"><FontAwesomeIcon icon="grin-stars" /></Link>ke</h3>
                        <h6 style={{ "fontFamily": "Shadows Into Light Two Regular" }} id="subHeading">A website to sing, listen, and emoji karaoke!</h6>
                    </Col>
                    <Col sm={{ size: 'auto' }} className="navElems">
                        <div>
                            <ul className="navLinkList">
                                <li><Link to="/"><FontAwesomeIcon icon="home" /></Link></li>
                                <li><NavLink to="/song_select"><FontAwesomeIcon icon="microphone-alt" /></NavLink></li>
                                {!localStorage.getItem('token')?<li><NavLink to="/login"><FontAwesomeIcon icon="sign-in-alt" /></NavLink></li>:""}
                                {!localStorage.getItem('token')?<li><NavLink to="/signup"><FontAwesomeIcon icon="user-plus" /></NavLink></li>:""}
                                <li><NavLink to="/profile"><FontAwesomeIcon icon="user" /></NavLink></li>
                            </ul>
                        </div>
                    </Col>
                </Row>
            </Container>
        </nav>
    )
}

export default Navbar;
