import * as React from 'react';
import './Home.css';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux'
import { ThunkDispatch, IRootState } from 'src/Store';
import { getSongRecord } from 'src/redux/songRecord/actions';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMousePointer } from '@fortawesome/free-solid-svg-icons';
library.add(faMousePointer);



interface ISongSelectState {
    isSelect: boolean
    selectedSong: string,
    dropdownOpen: boolean,
}

interface ISongSelectProps {
    recordList: any
    getAllRecord: () => void
}

class Home extends React.Component<ISongSelectProps, ISongSelectState> {
    constructor(props: ISongSelectProps) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.state = {
            isSelect: false,
            selectedSong: "",
            dropdownOpen: false,
        }
    }

    public toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    public async componentDidMount() {

        this.props.getAllRecord();

        this.setState({
            isSelect: false,
        })

    }

    public handleSubmit = (e: any) => {
        this.setState({
            isSelect: true,
            selectedSong: e.target.id
        })
    }

    public render() {
        return (
            <div id="homeBackgroundImage">
                <Dropdown id="dropdownHome" isOpen={this.state.dropdownOpen} size="sm" toggle={this.toggle}>
                    <DropdownToggle caret>
                        Filter Search
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem header>By Genre</DropdownItem>
                        <DropdownItem>Some Action</DropdownItem>
                        <DropdownItem disabled>Action (disabled)</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>Pop</DropdownItem>
                        <DropdownItem>Rock</DropdownItem>
                        <DropdownItem>Hip-Hop</DropdownItem>
                        <DropdownItem>EDM</DropdownItem>
                        <DropdownItem>Jazz</DropdownItem>
                        <DropdownItem>Classical</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem header>By Rating</DropdownItem>
                        <DropdownItem>Some Action</DropdownItem>
                        <DropdownItem disabled>Action (disabled)</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>5 Stars</DropdownItem>
                        <DropdownItem>4 Stars</DropdownItem>
                        <DropdownItem>3 Stars</DropdownItem>
                        <DropdownItem>2 Stars</DropdownItem>
                        <DropdownItem>1 Star</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
                <div className="homeSongSelectContainer">
                    <h5 id="welcomeSubHeading" className="text-white" style={{ "fontFamily": "Kenwave Regular" }}>Emoji karaoke, or sign up for an account to record yourself!</h5>
                    {
                        this.props.recordList.length > 1 ?
                            this.props.recordList.map((song: any, index: any) =>
                                <Container className="grid-container" id="homeBackgroundTexture" key={index}>
                                    <Row id ="homeSongSelectionRow">
                                        <Col sm={{ size: 'auto'}} className="grid-item songItem card">
                                            <div className="homeSongDiv responsive card-image">
                                                <img src={`${process.env.REACT_APP_API_SERVER}/images/cover_images/${song.cover_image}.jpg`} alt="music_cover_image" className="coverImg z-depth-2 hoverable" />
                                                <Container>
                                                    <h6 className="homeSongListText">{song.song_name}</h6>
                                                    <h6 style={{ "fontFamily": "Shadows Into Light Two Regular" }} className="homeSongListText">Emoji Rating: {song.avg_value.toFixed(2)}</h6>
                                                </Container>
                                            </div>
                                        </Col>

                                        <Col sm={{ size: 'auto' }} className="grid-item buttonItem">
                                            <div className="homeSongDiv">
                                                <img src={`${process.env.REACT_APP_API_SERVER}/images/profile_pictures/${song.profile_pic}`} alt="singer_profile_image" className="homeProfilePic" />
                                                {/* <h5 color="text-white" className="homeSongListText">{song.username}</h5> */}
                                            </div>
                                            <div className="homeButtonDiv" key={index}>
                                                <Link to={`video/${song.id}`}>
                                                    <Button color="info" className="homeSongListButton">
                                                        Select <FontAwesomeIcon icon="mouse-pointer" />
                                                    </Button>
                                                </Link>
                                            </div>
                                        </Col>
                                    </Row>
                                </Container>
                            )
                            : ''
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: IRootState) => ({
    recordList: state.recordList.songRecord
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getAllRecord: () => {
        dispatch(getSongRecord())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);