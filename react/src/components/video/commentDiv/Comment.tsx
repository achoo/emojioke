import * as React from 'react';
import './Comment.css';

import { connect } from 'react-redux';

import { ThunkDispatch, IRootState } from 'src/Store';
import { viewSongRecordComment } from 'src/redux/songRecordComment/actions';

class SongDetailBar extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }


  public componentDidMount() {
    this.props.viewSongRecordComment(this.props.videoId);
  }

  public insertComment(comment:string){
    this.props.insertComment(this.props.userID , this.props.videoId , comment)
  }

  public render() {

    return (
      <div className="container row">
        <div className="col s12 m12 ">
          <div className="chat row">
            <div className="chat-content row col">
              <div id="conversation" className="row col s12">
                {
                  this.props.comment.map((comment: any, index: any) =>
                    <div key={index} className="row" >
                      <div className="col s1">
                      <b key={index}>{comment.id}</b>
                      </div>
                      <div className="col s1">
                      <b>{comment.user_id}</b>
                      </div>
                      <div className="col s9">
                      <b>{comment.comments}</b>
                      </div>
                      <div className="col s1">
                      <b>{comment.time}</b>
                      </div>
                    </div>)
                }
              </div>
              <div id="convoInput" className="row col s12">
                <input id="data" />
                <input type="button" id="datasend" value="send" />
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  comment: state.songRecordComment.comments
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  viewSongRecordComment: (id: any) => {
    dispatch(viewSongRecordComment(id))
  }
  ,
  insertComment: (id: any) => {
    dispatch(viewSongRecordComment(id))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(SongDetailBar);