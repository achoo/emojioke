import * as React from 'react';
import './EmojiBar.css';
import EmojiIcon from './icon/EmojiIcon';
import returnIcon from 'src/components/Emoji'
import { Col } from 'reactstrap';
// import { connect } from 'react-redux';

class EmojiBar extends React.Component<any, any> {
   constructor(props: any) {
      super(props);
   }
   public render() {
      // const divStyle: any = {
      //    width: '200px',
      //    height: '200px'
      //  };
      //  const imgStyle: any = {
      //    maxWidth: "100%",
      //    maxHeight: "100%"
      //  };
      return (
         <Col sm={{ size: 6, offset: 3}}>
            {/* [1,2,3,4,5].sort(function(first,second){
                  return second -first;
             }) */}
            {

               this.props.emojiList.map((emojiIcon: any, index: any) =>
                  <div key={index} className="col s2">
                     < EmojiIcon addNewEmoji={this.props.addNewEmoji} getCurrentTime={this.props.currentTime}  userID={this.props.userID} emojiID={emojiIcon.id} videoID={this.props.videoId} src={returnIcon(emojiIcon.id)} tag={emojiIcon.type} />
                  </div>
               )

            }
         </Col>
      );
   }
}

export default EmojiBar;