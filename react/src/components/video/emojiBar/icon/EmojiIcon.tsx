import * as React from 'react';
import './EmojiIcon.css';
import { ThunkDispatch, IRootState } from 'src/Store';
import { connect } from 'react-redux'
import { addSongEmojiRecord } from 'src/redux/songRecordEmoji/actions';

class EmojiIcon extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }
  
  public addEmoji = () => {
    // need modify later
    const newEmoji = {
      emoji_id: this.props.emojiID,
      song_record_id: this.props.videoID,
      clicked_time: this.props.getCurrentTime(),
      clicked_coord_x: "3",
      clicked_coord_y: "3"
    }
    this.props.addSongEmojiRecord(this.props.userID, newEmoji);
    this.props.addNewEmoji(this.props.emojiID);
  }

  public render() {
    const imgStyle: any = {
      maxWidth: "100%",
      maxHeight: "100%"
    };
    return (
      <div style={imgStyle} >
        <img style={imgStyle} id={this.props.emojiID} src={this.props.src} alt={this.props.tag} className={this.props.tag} onClick={this.addEmoji} />
      </div>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  addSongEmojiRecord: (id: any, data: any) => {
    dispatch(addSongEmojiRecord(id, data))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(EmojiIcon);

// export default EmojiIcon;