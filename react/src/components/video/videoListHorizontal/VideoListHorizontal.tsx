import * as React from 'react';
import './VideoListHorizontal.css';

// // videoItem
// import VideoItem from './videoItem/VideoItem';

interface IVideoItemProps {
   user_id: number,
   song_id: number,
   filename: string
}

interface IVideoListProps {
   videos: IVideoItemProps[]
}

class VideoListHorizontal extends React.Component<IVideoListProps, {}>{

   constructor(props: IVideoListProps) {
      super(props)
   }

   public renderVideoItem(data: IVideoItemProps) {
      return (
         <div>
            <div>
               User ID : {data.user_id}
            </div>
            <div>
               Song ID : {data.song_id}
            </div>
            <div>
               filename : {data.filename}
            </div>
         </div>
      )
   }

   public render() {
      return (

         this.props.videos.map(i => (
            <div className="col s12 m6 x4 xl3">
               {
                  this.renderVideoItem(i)
               }
            </div>
         ))
      );
   }
}

export default VideoListHorizontal;