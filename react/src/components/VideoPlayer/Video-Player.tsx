import * as React from 'react';
import './Video-Player.css'

import Expire from 'src/components/Expire'

import { connect } from 'react-redux';

import { ThunkDispatch, IRootState } from 'src/Store';

import { viewSongEmojiRecord } from 'src/redux/songRecordEmoji/actions';

const { Player } = require('video-react');
import "node_modules/video-react/dist/video-react.css";

import Emoji from './Emoji/Emoji';
import returnIcon from 'src/components/Emoji'

// import bad from 'src/images/emoji/bad.png';

class VideoPlayer extends React.Component<any, any> {
  public player: any;
  public videoPlayer: any;
  public displayEmojiList: any = [];
  // public displayUserAddedEmoji: any = [];
  private maxCountOfShowingEmoji = 50;
  private emojiShowSecond = 3;
  constructor(props: any) {
    super(props);
    this.player = React.createRef();
    this.state = {
      displayEmojiList: [],
      emojiList: []
    };
  }

  public componentDidMount() {
    this.props.viewSongEmojiRecord(this.props.videoId);
    this.videoPlayer = this.player.current
    this.videoPlayer.subscribeToStateChange(this.handleStateChange.bind(this));
  }
  
  public componentDidUpdate(prevProps:any) {
    if (prevProps.src !== this.props.src) {
      this.player.current.video.load();
    }

  }

  public handleStateChange(state: any, prevState: any) {
    if (state.currentTime > prevState.currentTime) {
      const disPlayingEmoji = this.props.emojiRecord.data.filter((item: any, index: any, array: any) => {
        return (item.clicked_time >= prevState.currentTime && item.clicked_time <= state.currentTime);
      });
      disPlayingEmoji.map((emoji: any) => {
        emoji.clicked_coord_x = this.randomPosition(90);
        emoji.clicked_coord_y = this.randomPosition(80);
        const element = <Expire delay={(this.emojiShowSecond * 1000)}><Emoji Emoji={emoji} src={returnIcon(emoji.emoji_id)} /></Expire>
        if (this.displayEmojiList.length >= this.maxCountOfShowingEmoji) {
          this.displayEmojiList.shift();
        }
        this.displayEmojiList.push({ emoji: element, clicked_time: emoji.clicked_time });
        return emoji;
      })
      this.displayEmojiList = this.displayEmojiList.filter((item: any) => {
        return (item.clicked_time >= (state.currentTime - this.emojiShowSecond));
      });
    }

    if (state.duration === state.currentTime) {
      // alert("player is end !!!");
    }
    this.setState({
      player: state
    });
    // console.log(state);
    // console.log(this.displayUserAddedEmoji.length);
    if (state.currentTime !== undefined) {
      this.props.setCurrent(state.currentTime);
    }
  }


  public returnEmoji(id: any) {
    return this.displayEmojiList[id];
  }

  public randomPosition(max: number) {
    return Math.floor(Math.random() * Math.floor(max))
  }

  public render() {
    return (
      
      <Player ref={this.player} >
        <div id="emojiDiv" className="emojiDiv">
          {
            this.displayEmojiList.map((emoji: any, index: any) => {
              if (emoji !== undefined) {
                // return ""
                return emoji.emoji
                // return <Emoji key={index} Emoji={this.returnEmoji(index)} src={returnIcon(emoji.emoji_id)} />
              } else {
                return ""
              }
            })
          }

        </div>
        <source src={this.props.src} />
      </Player>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  emojiRecord: state.songEmojiRecord.emojiRecords
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  viewSongEmojiRecord: (id: any) => {
    dispatch(viewSongEmojiRecord(id));
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(VideoPlayer);

// export default VideoPlayer;







