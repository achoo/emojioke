import Button from "reactstrap/lib/Button";
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from './Store';
import { logout } from './redux/auth/action';
import * as React from 'react';

interface ILoginProps{
    isAuthenticated: boolean
    logout: ()=> void
}

class Logout extends React.Component<ILoginProps,{}> {
    constructor(props:ILoginProps){
        super(props);

    }
    private logout = ()=>{
        this.props.logout();
    }

    public render() {
        return (
        <div className="logout-bar">
            {
                this.props.isAuthenticated?
                <Button color="info" onClick={this.logout}>Logout</Button>:
                ""
            }
        </div>
        );
    }
}

const mapStateToProps = (state:IRootState)=>({
    isAuthenticated:  state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch:ThunkDispatch)=>({
    logout: ()=>dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
